The CiviCRM create contact record for each NEW Drupal user. If you install
CiviCRM in a site with existing users, you should syncronize Drupal user with
contacts: just go to civicrm/admin/synchUser?reset=1